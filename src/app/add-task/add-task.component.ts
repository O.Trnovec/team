import {Component} from '@angular/core';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {NgIf} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { TaskService } from '../services/task.service';
import { Tasks } from '../models/tasks.model';

@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.scss']
})
export class AddTaskComponent {
    constructor(public manager:TaskService){}
    value = "";
    public addTask() {
      let newTask: Tasks = {
        done: false,
        text: this.value
      }
      this.manager.addTask(newTask)
    }
    
}
