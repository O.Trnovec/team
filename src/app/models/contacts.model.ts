export interface Contacts{
    name: string
    email: string
    phone: number
    address?: string
    note?: string
}