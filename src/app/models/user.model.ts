export interface User{
    name: string
    email: string
    phone: number
    // notes: string (optional?)
}