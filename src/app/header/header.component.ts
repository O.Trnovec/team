import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  
})
export class HeaderComponent {
  @Input() public title: string = "TITLE";
  @Output() public sidebar: EventEmitter<void> = new EventEmitter<void>();
  public toggleSidebar(){
    this.sidebar.next();
  }
}
