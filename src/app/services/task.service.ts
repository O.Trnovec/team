import { Injectable } from '@angular/core';
import { Task } from '../checkboxes/checkboxes.component';
import { Tasks } from '../models/tasks.model';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
    private tasks: Tasks[]=[]
  constructor() {

   }

   public getTasks():Tasks[] {
    return this.tasks;
   }
   public addTask(task:Tasks):void {
    this.tasks.push(task);
   }
}