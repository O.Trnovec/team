import { Injectable } from '@angular/core';
import { Contacts } from '../models/contacts.model';

@Injectable({
  providedIn: 'root'
})
export class ContactsService {
    private contacts: Contacts[] = [
        {
            name: "gzgzug",
            email: "ýěýřěýěřýáě",
            phone: 2141151531
        },
        {   name: "gzáf",
            email: "dada",
            phone: 212344341531

        }
    ];


    public makeContact(name: any, email: any, phone: any, address: any, note: any): void{
        let newContact: Contacts = {
            name: name,
            email: email,
            phone: phone,
            address: address,
            note: note
        }
        this.contacts.push(newContact)
    }

    public getContacts(): Contacts [] {
        return this.contacts
    }
    


  constructor() { }
}
