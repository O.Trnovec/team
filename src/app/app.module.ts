import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProfileComponent } from './profile/profile.component';
import { CalendarComponent } from './calendar/calendar.component';
import { TasksComponent } from './tasks/tasks.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DropdownNewComponent } from './dropdown-new/dropdown-new.component';
import { DropdownSelectComponent } from './dropdown-select/dropdown-select.component';
import { FooterComponent } from './footer/footer.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { HeaderComponent } from './header/header.component';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { SidebarComponent } from './sidebar/sidebar.component';
import { NgIf } from '@angular/common';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import { CheckboxesComponent } from './checkboxes/checkboxes.component';
import { MiniCalendarComponent } from './mini-calendar/mini-calendar.component';
import { DateOnTasksComponent } from './date-on-tasks/date-on-tasks.component';
import { ArrowsComponent } from './arrows/arrows.component';
import { MatInputModule } from '@angular/material/input';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AddTaskComponent } from './add-task/add-task.component';
import { FormsModule } from '@angular/forms'

@NgModule({
  declarations: [
    AppComponent,
    ProfileComponent,
    CalendarComponent,
    TasksComponent,
    ContactsComponent,
    DropdownNewComponent,
    DropdownSelectComponent,
    FooterComponent,
    HeaderComponent,
    SidebarComponent,
    CheckboxesComponent,
    MiniCalendarComponent,
    DateOnTasksComponent,
    ArrowsComponent,
    AddTaskComponent,
    
  ],
  imports: [
    MatFormFieldModule, MatSelectModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule, MatButtonModule, MatIconModule,//header
    MatSidenavModule, NgIf, MatButtonModule, //sidebar
    MatDatepickerModule, MatNativeDateModule,MatFormFieldModule,
    MatInputModule,MatButtonModule, MatCheckboxModule,
    MatListModule, FormsModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
export class DividerOverviewExample {}
