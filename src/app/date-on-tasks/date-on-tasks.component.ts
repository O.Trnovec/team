import { Component } from '@angular/core';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-date-on-tasks',
  templateUrl: './date-on-tasks.component.html',
  styleUrls: ['./date-on-tasks.component.scss']
})
export class DateOnTasksComponent {
  currentDate: Date = new Date();

}
