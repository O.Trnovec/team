import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DateOnTasksComponent } from './date-on-tasks.component';

describe('DateOnTasksComponent', () => {
  let component: DateOnTasksComponent;
  let fixture: ComponentFixture<DateOnTasksComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DateOnTasksComponent]
    });
    fixture = TestBed.createComponent(DateOnTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
