import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CalendarComponent } from './calendar/calendar.component';
import { ContactsComponent } from './contacts/contacts.component';
import { TasksComponent } from './tasks/tasks.component';
import { ProfileComponent } from './profile/profile.component';
import { DropdownNewComponent } from './dropdown-new/dropdown-new.component'
import { AddTaskComponent } from './add-task/add-task.component';

const routes: Routes = [
  {
    component: CalendarComponent,
    path: "calendar",
    data: {
      title: "Calendar"
    }
  },
  {
    component: AddTaskComponent,
    path: "add",
    data: {
      title: "New task"
    }
  },
  {
    component: ContactsComponent,
    path: "contacts",
    data: {
      title: "Contacts"
    }
  },
  {
    component: TasksComponent,
    path: "tasks",
    data: {
      title: "To Do"
    }
  },
  {
    component: ProfileComponent,
    path: "profile",
    data: {
      title: "Profile"
    }
  },

  {
    redirectTo: "tasks",
    path: "",
    pathMatch: "full"
   
    }
  
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
