import { Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {
  @Output() public toDo: EventEmitter<void> = new EventEmitter<void>();
  showFiller = false;
  toggle: boolean = false;
  
  public toggleSidebar(){
    this.toggle=!this.toggle;
  
  }
  public goToDo(){
    this.toDo.next();
  
  }
}
