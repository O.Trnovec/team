import { Component, EventEmitter, Output } from '@angular/core';
import { ContactsService } from '../services/contacts.service';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent {
  
  constructor(public manager:ContactsService){}
  @Output() public contactDetails: EventEmitter<void> = new EventEmitter<void>();
  public openContactDetails(){
    this.contactDetails.next();
}
}